# cc-radioBtnBox


#### 使用方法 
```使用方法
<!-- attrArr：属性数据 selIndexArr:选择序列数组 不设置默认不选中 @click：属性选择事件 返回属性选择序列数组  -->
<cc-radioBtnBox :attrArr="attrArr" :selIndexArr="selIndexArr" @click="selectAttrClick"></cc-radioBtnBox>
```

#### HTML代码实现部分
```html
<template>
	<view class="page">
		<!-- attrArr：属性数据 selIndexArr:选择序列数组 不设置默认不选中 @click：属性选择事件 返回属性选择序列数组  -->
		<cc-radioBtnBox :attrArr="attrArr" :selIndexArr="selIndexArr" @click="selectAttrClick"></cc-radioBtnBox>

	</view>
</template>

<script>
	export default {

		data() {
			return {
				// 设置都选择第一个
				selIndexArr: [0, 0, 0, 0],
				attrArr: [{
						attr: '系列',
						value: ['iphone 14系列', 'iphone 14 Pro系列']
					},
					{
						attr: '版本',
						value: ['128GB', '256GB', '512GB']
					},
					{
						attr: '颜色',
						value: ['午夜色', '星光色', '紫色', '蓝色', '红色', '黄色']
					},
					{
						attr: '白条',
						value: ['不分期', '3期', '6期', '12期']
					},
				],
			};
		},


		methods: {

			selectAttrClick(value) {

				console.log("选择属性的值 = " + value);
				uni.showModal({
					title: '选择属性的值',
					content: '选择属性的值 = ' + value
				})
			},

		}
	}
</script>

<style scoped lang="scss">
	page {

		padding-bottom: 70px;
	}
</style>

```
