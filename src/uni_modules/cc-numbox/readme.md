# cc-numbox


#### 使用方法

```使用方法
<!-- title: 标题  isSetMax: 是否设置最大值  maxNum: 最大值-->
<cc-numbox title="商品数量(设置最大值)" :isSetMax="true" maxNum="20" 

```

#### HTML代码实现部分
```html

<template>
	<view class="content">

		<view style="height: 20px;"></view>
		<!-- title: 标题  isSetMax: 是否设置最大值  maxNum: 最大值-->
		<cc-numbox title="基本用法" @change="numChangeClick"></cc-numbox>

		<view style="height: 20px;"></view>
		<!-- title: 标题  isSetMax: 是否设置最大值  maxNum: 最大值-->
		<cc-numbox title="商品数量(设置最大值)" :isSetMax="true" maxNum="20" @change="numChangeClick"></cc-numbox>



	</view>
</template>

<script>
	export default {
		components: {

		},
		data() {
			return {

			}
		},
		methods: {

			numChangeClick(num) {

				console.log("当前数量 = " + num);

			}
		}
	}
</script>

<style>
	.content {
		display: flex;
		flex-direction: column;

	}
</style>




```
