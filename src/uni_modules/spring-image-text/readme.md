### 注意事项

``注意事项``  本组件使用了``uni-icons``, 请在使用前先引用``uni-icons``组件。本组件使用了iconfont图标, 如若更换图标，请自行上传iconfont.css进行更换。


### 安装方式

本组件符合[easycom](https://uniapp.dcloud.io/collocation/pages?id=easycom)规范，`HBuilderX 2.5.5`起，只需将本组件导入项目，在页面`template`中即可直接使用，无需在页面中`import`和注册`components`。


### 基本用法

在 ``template`` 中使用组件

```html
<spring-image-text :dataList="list"></spring-image-text>
```

### 图片尺寸1:1

```html
<spring-image-text :dataList="list" imageShape="square"></spring-image-text>
```


### 图片位置更换

```html
<spring-image-text :dataList="list" imagePosition="right"></spring-image-text>
```

### 只显示标题

```html
<spring-image-text :dataList="list" :showDesc="false"></spring-image-text>
```



```javascript

export default {
	data() {
		return {
			list: [{
					coverImage: 'https://docreview.oss-cn-hangzhou.aliyuncs.com/doc-review/66417e0c60b26b8d682a1bcd.png',
					title: '云南栖花岭',
					desc: '云南栖花岭',
					createTime: '2024-05-01 10:00'
				},
				{
					coverImage: 'https://docreview.oss-cn-hangzhou.aliyuncs.com/doc-review/66417e0c60b26b8d682a1bcd.png',
					title: '天然石',
					desc: '我是个小小的石头',
					createTime: '2024-05-01 10:00'
				},
				{
					coverImage: 'https://tennis-1253685553.cos.ap-shanghai.myqcloud.com/tennis/662da982d5dee7df76a12b49.png',
					title: '活动风景',
					desc: '这个关于风景的摄影展',
					createTime: '2024-05-01 10:00'
				}
			]
		}
	},
	onReachBottom() {
		console.log('到底了！');
	},
	onLoad() {
		uni.setNavigationBarTitle({
			title: '只显示标题'
		})
	},
	methods: {

	}
}

```


## API

###  Props

|  属性名					|    类型	| 默认值		| 说明																																								|
|									|					|
| dataList				| Array		|		-			| 数据，期望数据格式columns: [{coverImage: '', title:'', desc: '', createTime: ''}...]	|
| showDesc				| Boolean	| true		| 是否显示描述																																				|
| showTimeIcon		| Boolean	| false		| 是否显示时间图标																																			|
| radius					| String	| 16rpx		|	外框圆角																																						|
| imageRadius			| String	| 16rpx		|	外框圆角																																						|
| titleFontWeight	| Number	| 700			|	标题字体粗细																																				|
| bgColor					| String	| #D4E5EF	| 背景色																																							|
| color						| String	|	#354E6B	| 文字颜色																																						|
| descColor				| String	|	-				| 描述文字颜色，如不填，则与color保持一致																									|
| timeColor				| String	|	-				| 时间文字和图标颜色，如不填，则与color保持一致																						|
| imagePosition		| String	|	left		| 图片位置																																						|
| imageShape			| String	|	-				| 图片形状，可填项 square																															|


###  Events

|  事件名	| 说明			|	返回值				|
|					|					|							|
| tapItem	| 点击事件	| 返回点击的对象	|