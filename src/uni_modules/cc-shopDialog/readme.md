# cc-shopDialog

#### 使用方法 

使用注意： 该插件需引用cc-radioBtnBox插件和cc-numbox插件两个插件库

cc-radioBtnBox插件地址：https://ext.dcloud.net.cn/plugin?id=13176

cc-numbox插件地址：https://ext.dcloud.net.cn/plugin?id=13163

```使用方法
<!-- dialogHeight：弹框高度 shopItem:商品数据  @close：关闭弹框 @toCart：加入购物车事件返回当前shopitem数据 @toBuy：立即购买事件返回当前shopitem数据  -->
<cc-shopDialog class="hidden" :dialogHeight="440" :shopItem="shopItem" @close="closeShopDialog" @toCart="toCart"
@toBuy="toBuy" :class="{show:shopFlag}"></cc-shopDialog>

```

#### HTML代码实现部分
```html

<template>
	<view class="page">

		<button class="btnV" @click="showShopDialog">显示购物车</button>

		<!-- dialogHeight：弹框高度 shopItem:商品数据  @close：关闭弹框 @toCart：加入购物车事件返回当前shopitem数据 @toBuy：立即购买事件返回当前shopitem数据  -->
		<cc-shopDialog class="hidden" :dialogHeight="440" :shopItem="shopItem" @close="closeShopDialog" @toCart="toCart"
			@toBuy="toBuy" :class="{show:shopFlag}"></cc-shopDialog>

	</view>
</template>

<script>
	export default {

		data() {
			return {
				shopFlag: false,
				shopItem: {
					'imgUrl': 'https://cdn.pixabay.com/photo/2014/08/05/10/30/iphone-410324_1280.jpg', //图片地址
					'price': '', // 价格 通过计算得来
					'attrTxt': '', //属性文本  通过计算得来
					'num': '1', // 购买数量
					'maxNum': '60', // 购买最大数量

					// 属性数据
					'attrArr': [{
							attr: '系列',
							value: ['iphone 14系列', 'iphone 14 Pro系列']
						},
						{
							attr: '版本',
							value: ['256GB', '512GB']
						}
					],
					'selAttrArr': [0, 0], //选择的属性序列数组
					// 价格字典 根据属性组合对应价格
					'priceDict': {
						'iphone 14系列,256GB': '5600',
						'iphone 14系列,512GB': '6400',
						'iphone 14 Pro系列,256GB': '6200',
						'iphone 14 Pro系列,512GB': '6900'
					},

				}


			};
		},


		methods: {

			//显示购买弹窗
			showShopDialog() {
				this.shopFlag = true
			},
			closeShopDialog() {
				this.shopFlag = false
			},
			toCart(item) {

				console.log('加入购物车商品数据 = ' + JSON.stringify(item))
				uni.showModal({
					title: '加入购物车',
					content: '加入购物车商品数据 = ' + JSON.stringify(item)
				})
			},
			toBuy(item) {

				console.log('立即购买商品数据 = ' + JSON.stringify(item))
				uni.showModal({
					title: '立即购买',
					content: '立即购买商品数据 = ' + JSON.stringify(item)
				})

			},


		}
	}
</script>

<style scoped lang="scss">
	page {

		padding-bottom: 70px;
	}

	.btnV {

		width: 180px;
		height: 44px;
		margin-top: 36px;
	}

	.hidden {
		display: none;
	}

	.show {
		display: block;
	}
</style>


```
