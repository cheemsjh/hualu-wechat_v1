import service from "../index";


export const getName = (data) => {
   
  return service({
    url: "/wxApp/getWelds",
    method: "POST",
    data,
  });
    return new Promise((res) => {
      res({
        code: 200,
        data: [
           {
            id:'xxx',
            name:'焊口：1',
           },
           {
            id:'xxx',
            name:'焊口：2',
           },
           {
            id:'xxx',
            name:'焊口：3',
           },
           {
            id:'xxx',
            name:'焊口：4',
           },

           {
            id:'xxx',
            name:'焊口：5',
           },
           {
            id:'xxx',
            name:'焊口：36',
           },
           {
            id:'xxx',
            name:'焊口：7',
           },
           {
            id:'xxx',
            name:'焊口：8',
           },
           {
            id:'xxx',
            name:'焊口：9',
           },
           {
            id:'xxx',
            name:'焊口：10',
           },
           {
            id:'xxx',
            name:'焊口：3',
           },
           {
            id:'xxx',
            name:'焊口：3',
           },
           {
            id:'xxx',
            name:'焊口：3',
           },
           {
            id:'xxx',
            name:'焊口：3',
           },
           {
            id:'xxx',
            name:'焊口：3',
           },
           {
            id:'xxx',
            name:'焊口：3',
           },
           {
            id:'xxx',
            name:'焊口：3',
           },
           {
            id:'xxx',
            name:'焊口：3',
           },
           {
            id:'xxx',
            name:'焊口：3',
           },
           {
            id:'xxx',
            name:'焊口：3',
           },
           {
            id:'xxx',
            name:'焊口：3',
           },
        ],
      });
    });
  };



  export const creater = (data) => {
    return service({
      url: "/wxApp/getWeldInfo",
      method: "POST",
      data,
    });
    return new Promise((res) => {
      res({
        code: 200,
        data: {
           num:'SDDW2-33', //牌号
           weldingPosition:'weldingPosition', //焊接位置
           weldingManner:'weldingManner',//焊接方式
          date:'2022-13-14'
        },
      });
    });
  };


  export const selectOptions1 = (data) => {
   
    return service({
      url: "/wxApp/getRodDic",
      method: "POST",
      data,
    });
      
    };


    export const selectOptions2 = (data) => {
   
      return service({
        url: "/wxApp/getPosDic",
        method: "POST",
        data,
      });
        
      };


      export const selectOptions3 = (data) => {
   
        return service({
          url: "/wxApp/getMethodDic",
          method: "POST",
          data,
        });
          
        };


        export const updateWeld = (data) => {
   
          return service({
            url: "/wxApp/updateWeld",
            method: "POST",
            data,
          });
            
          };


        
      export const selectOptions4 = (data) => {
   
        return service({
          url: "/wxApp/getWeldUsers",
          method: "POST",
          data,
        });
          
        };