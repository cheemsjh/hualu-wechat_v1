import service from "../index";

export const login = (data) => {
  return service({
      method:'POST',
      url:'/wxApp/login',
      data
  })


};

export const getWeldByQrcode = (data) => {
  return service({
      method:'POST',
      url:'/wxApp/getWeldByQrcode',
      data
  })


};
