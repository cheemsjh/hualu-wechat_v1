import axios from "axios";


import adapter from 'axios-miniprogram-adapter'
// 创建一个 axios 实例
const service = axios.create({
  adapter: adapter,
  baseURL: "https://www.hualuweld.top:48080/apt", // 所有的请求地址前缀部分
  timeout: 600000, // 请求超时时间毫秒
  withCredentials: true, // 异步请求携带cookie
  transformResponse: [
    function (data) {
    
      // 对 data 进行任意转换处理
       return data;
    },
  ],


});

// 添加请求拦截器
service.interceptors.request.use(
  (config) => {
   if(uni.getStorageSync('satoken')){
       
    config.headers["satoken"] = uni.getStorageSync('satoken') // 设置请求头
    
   }
    return config;
  },

  (err) => {
    return Promise.reject(err);
  },
);

// 添加响应拦截器
service.interceptors.response.use(
  (res) => {
    console.log(res)
    if(res.data.code === 403){
      uni.showToast({
        title: '登录失效，请重新登录',
        icon: 'none',
        duration: 2000
      });
      uni.navigateTo({ url: '/pages/index/login' })
    }
   return res.data;
  },
  (err) => {
    return Promise.reject(err);
  },
);

export default service;
